/* See LICENSE file for copyright and license details. */

/* appearance */
static const char *fonts[] = { "monospace:bold:size=15" };
static const char dmenufont[] = "monospace:size=15";
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char col_gray1[]       = "#375EAB";
static const char col_gray2[]       = "#AABCE0";
static const char col_gray3[]       = "#E0EBF5";
static const char col_gray4[]       = "#FFFFFF";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray1, col_gray2, col_gray2 },
	[SchemeSel] =  { col_gray4, col_gray1, col_gray3 },
};

/* tagging */
static const char *tags[] = { "1", "2", "3",
			      "4", "5", "6",
			      "7", "8", "9", };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 * format:
	 *	class / instance / title / tags mask / isfloating / monitor
	 */
	{ "console", NULL, "st", 0, 1, -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

/* first entry is default */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "[O]",      monocle },
	{ "^-^",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,             KEY, toggleview, {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask, KEY, view,       {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask,    KEY, toggletag,  {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,   KEY, tag,        {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont,
				  "-nb", col_gray2, "-nf", col_gray1,
				  "-sb", col_gray1, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", "-c", "console", "-g", "80x24", NULL };
static const char *tmuxcmd[]  = { "st", "tmux", "-u",
				  "new-session", "-A", "-D", "-s", "main", ";",
				  "new-session", "-A", "-D", "-s", "help", ";",
				  "switch-client", "-p",
				  NULL };
static const char  *chromiumcmd[] = { "chromium", "--user-data-dir=.config/chromium", NULL };

/* XKB_KEY_XF86Audio* definitions */
#include <xkbcommon/xkbcommon-keysyms.h>

static Key keys[] = {
	/* modifier           key              function        argument */
	{ MODKEY,             XK_p,            spawn,          {.v = dmenucmd} },
	{ MODKEY,             XK_backslash,    spawn,          {.v = tmuxcmd} },
	{ MODKEY,             XK_grave,        spawn,          {.v = termcmd} },
	{ MODKEY,             XK_i,            spawn,          {.v = chromiumcmd} },
	{ MODKEY,             XK_b,            togglebar,      {0} },
	{ MODKEY,             XK_Up,           incnmaster,     {.i = +1} },
	{ MODKEY,             XK_Down,         incnmaster,     {.i = -1} },
	{ MODKEY,             XK_Left,         setmfact,       {.f = -0.05} },
	{ MODKEY,             XK_Right,        setmfact,       {.f = +0.05} },
	{ MODKEY,             XK_bracketright, focusstack,     {.i = +1} },
	{ MODKEY,             XK_bracketleft,  focusstack,     {.i = -1} },
	{ MODKEY,             XK_BackSpace,    view,           {0} },
	{ MODKEY|ShiftMask,   XK_Return,       togglefloating, {0} },
	{ MODKEY,             XK_Return,       zoom,           {0} },
	{ MODKEY,             XK_space,        setlayout,      {0} },
	{ MODKEY,             XK_t,            setlayout,      {.v = &layouts[0]} },
	{ MODKEY,             XK_m,            setlayout,      {.v = &layouts[1]} },
	{ MODKEY,             XK_f,            setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ControlMask, XK_0,            view,           {.ui = ~0} },
	{ MODKEY|ShiftMask,   XK_0,            tag,            {.ui = ~0} },
	TAGKEYS(              XK_1,                            0)
	TAGKEYS(              XK_2,                            1)
	TAGKEYS(              XK_3,                            2)
	TAGKEYS(              XK_4,                            3)
	TAGKEYS(              XK_5,                            4)
	TAGKEYS(              XK_6,                            5)
	TAGKEYS(              XK_7,                            6)
	TAGKEYS(              XK_8,                            7)
	TAGKEYS(              XK_9,                            8)
	{ MODKEY|ShiftMask,   XK_w,            killclient,     {0} },

	{ MODKEY,             XK_period,       focusmon,       {.i = +1 } },
	{ MODKEY,             XK_comma,        focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,   XK_period,       tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,   XK_comma,        tagmon,         {.i = -1 } },

	{ MODKEY|ControlMask|ShiftMask, XK_Escape, quit, {0} },

	{ 0, XKB_KEY_XF86AudioRaiseVolume, spawn, SHCMD("pactl set-sink-mute 0 false ; pactl set-sink-volume 0 +5%") },
	{ 0, XKB_KEY_XF86AudioLowerVolume, spawn, SHCMD("pactl set-sink-mute 0 false ; pactl set-sink-volume 0 -5%") },
	{ 0, XKB_KEY_XF86AudioMute,        spawn, {.v = (const char*[]){ "pactl", "set-sink-mute", "0", "1", NULL }}},
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click              event mask       button          function        argument */
	{ ClkWinTitle,        0,               Button1,        zoom,           {0} },
	{ ClkWinTitle,        0,               Button2,        togglefloating, {0} },
	{ ClkWinTitle,        MODKEY,          Button2,        killclient,     {0} },
	{ ClkWinTitle,        0,               Button3,        view,           {0} },
	{ ClkClientWin,       MODKEY,          Button1,        zoom,           {0} },
	{ ClkClientWin,       MODKEY,          Button2,        movemouse,      {0} },
	{ ClkClientWin,       MODKEY,          Button3,        resizemouse,    {0} },
	{ ClkTagBar,          0,               Button1,        toggleview,     {0} },
	{ ClkTagBar,          0,               Button2,        tag,            {0} },
	{ ClkTagBar,          0,               Button3,        toggletag,      {0} },
	{ ClkTagBar,          MODKEY,          Button1,        view,           {0} },
	{ ClkLtSymbol,        0,               Button1,        setlayout,      {0} },
};
